import sirv from 'sirv';
import polka from 'polka';
import uuidv4 from 'uuid/v4';
import helmet from 'helmet';
import compression from 'compression';
import * as sapper from '@sapper/server';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

require('dotenv/config')


polka() 
	.use(( req, res, next )=>{
			res.locals.nonce = uuidv4();
			next();
	});

polka()
	.use(
		helmet({
			contentSecurityPolicy: { 
				directives: {
					defaultSrc:[	
						"'self'",
					],
					scriptSrc:[ 
					    "'self'",
						( req, res ) => `'nonce-${ res.locals.nonce }'`
					],
					styleSrc:[
					    "'self'",
					]
				}, 
			},
		})
	);

polka()
	.use(
		"/M4dh0u3e",
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware()
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});
