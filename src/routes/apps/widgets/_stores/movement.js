import { writable } from 'svelte/store';

function selectWidget(){
	const { subscribe, set, update } = writable( Object() );

	return {
		subscribe, 
		select: ( e, comp )=> {
			update( selected => {
				return { 
					id: comp.id,   
					name: comp.name,
					offsetX: e.clientX  - parseFloat( comp.cood.x ),
					offsetY: e.clientY  - parseFloat( comp.cood.y ), 
				}
			})
		},
		reset: () => set( new Object() )
	};
};

export const selected = selectWidget();

function positionWidget(){
	const { subscribe, set, update } = writable( Object() );

	return {
		subscribe, 
		move: ( e, cood )=> {
			e.preventDefault();
			update( position => {
				return { 
					x: e.clientX  - cood.offsetX,
					y: e.clientY  - cood.offsetY,
					z: 2,
				}
			})

		},
		reset: () => set( new Object() )
	};
};

export const position = positionWidget();
