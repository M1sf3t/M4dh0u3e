import { graphData as data }  from './temps.js';
import { derived } from 'svelte/store'

export const lineGraph = derived( data, $data => {
	const { scaleX, scaleY, intX, intY, dataX, dataY } = $data;

	const { y1, y2, x1, x2 } = {
		y1 : Math.floor( Math.min( ...dataY )/10 )*10,
		y2 : Math.ceil( Math.max( ...dataY )/10 )*10,
		x1 : 0,
		x2 : dataX.length,
	};

	const yAxis = Array( Math.ceil(( y2-y1 )/intY+1 )).fill( scaleY ).map(( tick, i )=> y1+tick*i );
	const xAxis = Array( Math.ceil(( x2-x1 )/intX )).fill( scaleX ).map(( tick, i )=> x1+tick*i );

	return {
		y1,
		y2,
		x1,
		x2,
		yAxis,
		xAxis, 
		yInput: Array.from( yAxis ).reverse(),
		xInput: Array.from( dataX ),
		points: dataY.map(( temp, i )=>[ xAxis[i],temp ]),
		line: dataY.map(( temp, i )=>`${xAxis[i]},${temp}`).join(" "),
	};
})
