import { writable, derived } from 'svelte/store';

export const game = writable({
	w: 200,
	h: 100,
	x: 0,
	y: 0,
	lives: 3,
	score: 0,
	level: 1,
	bricks: { rows:4, cols:10, h:5, w:10, x:10, y:10, p:5 },
});

const drawBricks = ( brick )=> Array.from(
	{ length: brick.rows }, 
	( _, row ) => Array.from(
		{ length: brick.cols }, 
		( _, col ) => {
			let x = col*( brick.w +brick.x)+brick.p;
			let y = row*( brick.h+brick.y)+brick.p;
			let id = x+","+y;
			return { id, x, y, h:brick.h, w:brick.w };
		}
	)
);

export const bricks = derived(
	game,
	$game => drawBricks( $game.bricks ).flat()
);


function drawBall(){
	const { subscribe, set, update } = writable({ x:100, y:80, r:5, sx:1, sy:1 });
	return {
		subscribe,
		start: ()=> update( cood =>{ 
			let x = cood.x+cood.sx;
			let y = cood.y-cood.sy;
			let r = cood.r;
			let sx = cood.sx;
			let sy = cood.sy;
			return { x, y, r, sx, sy };
		}),
		flipX: ()=> update( cood =>{ 
			let x = cood.x;
			let y = cood.y;
			let r = cood.r;
			let sx = -cood.sx;
			let sy = cood.sy;
			return { x, y, r, sx, sy }
		}),
		flipY: ()=> update( cood =>{ 
			let x = cood.x;
			let y = cood.y;
			let r = cood.r;
			let sx = cood.sx;
			let sy = -cood.sy;
			return { x, y, r, sx, sy }
		}),
		reset: ()=> set({ x:100, y:80, r:5, sx:1, sy:1 })
	};
};

export const ball = drawBall();

function drawPaddle(){
	const { subscribe, set, update } = writable({ x:85, y:85, h:5, w:30, s:7 });
	return {
		subscribe,
		moveL: ()=> update( cood =>{ 
			let x = cood.x-cood.s;
			let y = cood.y;
			let w = cood.w;
			let h = cood.h;
			let s = cood.s;
			return { x, y, w, h, s }
		}),
		moveR: ()=> update( cood =>{ 
			let x = cood.x+cood.s;
			let y = cood.y;
			let w = cood.w;
			let h = cood.h;
			let s = cood.s;
			return { x, y, w, h, s }
		}),
		reset: ()=> set({ x:85, y:85, h:5, w:30, s: 7 })
	};
};

export const paddle = drawPaddle();
