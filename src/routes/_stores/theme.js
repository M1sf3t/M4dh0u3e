import { writable, derived } from 'svelte/store';

function setTheme(){
	const { subscribe, update } = writable('light');

	return {
		subscribe,
		dark: ()=> update( 
			n => 'dark'	
		),
		light: ()=> update( 
			n => 'light' 
		)
	};
};

export const Theme = setTheme();

