import { writable } from 'svelte/store';

function createToggle(){
	const { subscribe, set, update } = writable(0);

	return {
		subscribe,
		left: ()=> update( val => val -= 1 ),
		right: ()=> update( val => val += 1 ),
		begin: ()=> set( 0 ),
		end: ( l )=> set( l-1 )
	};
}

export const toggle = createToggle();
